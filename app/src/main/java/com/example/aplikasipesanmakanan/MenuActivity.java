package com.example.aplikasipesanmakanan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

public class MenuActivity extends AppCompatActivity {

    String namaUser;
    TextView txtNama;

    FirebaseUser user;
    FirebaseAuth mAuth;

    FirebaseFirestore fireDb;

    private RecyclerView recMakanan;
    private RecyclerView recMainan;
    private ArrayList<Makanan> listMakanan;
    private ArrayList<Mainan> listMainan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        initFab();

        fireDb = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        namaUser = user.getEmail();

        recMakanan = findViewById(R.id.rec_makanan);
        recMainan = findViewById((R.id.rec_mainan));
        initDataMakanan();
        initDataMainan();

        recMakanan.setAdapter(new MakananAdapter(listMakanan));
        recMakanan.setLayoutManager(new LinearLayoutManager(this));

        recMainan.setAdapter(new MainanAdapter(listMainan));
        recMainan.setLayoutManager(new LinearLayoutManager(this));

        txtNama = findViewById(R.id.txtNama);
        txtNama.setText("Hi, " + namaUser);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initFab(){
        FloatingActionButton fabCart = findViewById(R.id.fabCart);
        fabCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getBaseContext(), CartActivity.class));
            }
        });

        FloatingActionButton fabLogout = findViewById(R.id.fabLogout);
        fabLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAuth.signOut();
                startActivity(new Intent(getBaseContext(), MainActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });
    }

    private void initDataMakanan(){
        this.listMakanan =  new ArrayList<>();
        listMakanan.add(new Makanan("Makanan Kering (dry food)", "30000", R.drawable.dry_food));
        listMakanan.add(new Makanan("Makanan Basah (wet food)", "40000", R.drawable.wet_food));
        listMakanan.add(new Makanan("Jerami", "20000", R.drawable.jerami));
        listMakanan.add(new Makanan("Pelet Ikan", "15000", R.drawable.pelet_ikan));
        listMakanan.add(new Makanan("Makanan Burung", "20000", R.drawable.makanan_burung));
        listMakanan.add(new Makanan("Makanan Serangga", "15000", R.drawable.pakan_serangga));
    }

    private void initDataMainan(){
        this.listMainan = new ArrayList<>();
        listMainan.add(new Mainan("Bola", "15000", R.drawable.bola));
        listMainan.add(new Mainan("Mainan Interaktif", "20000", R.drawable.mainan_interaktif));
        listMainan.add(new Mainan("Mainan Rangsang Mental", "25000", R.drawable.mainan_rangsangmental));
        listMainan.add(new Mainan("Mainan Berbulu", "10000", R.drawable.mainan_bulu));
        listMainan.add(new Mainan("Gigitan", "12000", R.drawable.gigitananjing));
        listMainan.add(new Mainan("Mainan Laser", "30000", R.drawable.laser_kucing));
    }
}