package com.example.aplikasipesanmakanan;

public class Makanan {
    private String namaMakanan;
    String hargaMakanan;
    private int imgMakanan;

    public Makanan(String namaPenyetan, String hargaPenyetan, int imgPenyetan) {
        this.namaMakanan = namaPenyetan;
        this.hargaMakanan = hargaPenyetan;
        this.imgMakanan= imgPenyetan;
    }

    public String getNamaMakanan() {
        return namaMakanan;
    }
    public void setNamaMakanan(String namaMakanan){this.namaMakanan = namaMakanan;}
    public String getHargaMakanan() {
        return hargaMakanan;
    }
    public void setHargaMakanan(String hargaPenyetan) {
        this.hargaMakanan = hargaPenyetan;
    }
    public int getImgMakanan(){return imgMakanan;}
    public void setImgMakanan(int imgMakanan) {
        this.imgMakanan = imgMakanan;
    }
}
