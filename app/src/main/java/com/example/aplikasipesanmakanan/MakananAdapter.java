package com.example.aplikasipesanmakanan;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class MakananAdapter extends RecyclerView.Adapter<MakananAdapter.ViewHolder> {

    ViewHolder holder;

    public MakananAdapter(ArrayList<Makanan> listMakanan) {
        this.listMakanan = listMakanan;
    }

    private ArrayList<Makanan> listMakanan;

    @NonNull
    @Override
    public MakananAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewHolder holder =  new ViewHolder(inflater.inflate(R.layout.item_menu, parent, false));

        return holder;
    }

    public String rp(int txt){
        Locale locale = new Locale("in", "ID");
        NumberFormat format = NumberFormat.getCurrencyInstance(locale);
        format.setMaximumFractionDigits(0);
        return format.format(txt); // Integer.toString(total);
    }

    @Override
    public void onBindViewHolder(@NonNull MakananAdapter.ViewHolder holder, int position) {
        Makanan makanan = listMakanan.get(position);
        holder.txtNamaMakanan.setText(makanan.getNamaMakanan());
        holder.txtHargaMakanan.setText(rp(Integer.parseInt(makanan.getHargaMakanan())));
        holder.imgMakanan.setImageResource(makanan.getImgMakanan());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listMakanan.get(position).getNamaMakanan().equals("Makanan Kering (dry food)")) {
                    Intent intent = new Intent(holder.itemView.getContext(), MenuDetailActivity.class);
                    intent.putExtra("GAMBAR_DEFAULT", R.drawable.dry_food);
                    intent.putExtra("NAMA_DEFAULT", "Makanan Kering (dry food)");
                    intent.putExtra("DESKRIPSI_DEFAULT", "Kibbles gurih yang memberikan nutrisi lengkap untuk hewan peliharaan Anda.");
                    intent.putExtra("HARGA_DEFAULT", "30000");
                    holder.itemView.getContext().startActivity(intent);
                }
                if (listMakanan.get(position).getNamaMakanan().equals("Makanan Basah (wet food)")) {
                    Intent intent = new Intent(holder.itemView.getContext(), MenuDetailActivity.class);
                    intent.putExtra("GAMBAR_DEFAULT", R.drawable.wet_food);
                    intent.putExtra("NAMA_DEFAULT", "Makanan Basah (wet food)");
                    intent.putExtra("DESKRIPSI_DEFAULT", "Makanan dalam kaleng yang menggugah selera untuk hewan peliharaan yang rewel");
                    intent.putExtra("HARGA_DEFAULT", "40000");
                    holder.itemView.getContext().startActivity(intent);
                }
                if (listMakanan.get(position).getNamaMakanan().equals("Jerami")) {
                    Intent intent = new Intent(holder.itemView.getContext(), MenuDetailActivity.class);
                    intent.putExtra("GAMBAR_DEFAULT", R.drawable.jerami);
                    intent.putExtra("NAMA_DEFAULT", "Jerami");
                    intent.putExtra("DESKRIPSI_DEFAULT", "Membantu kelinci dan hamster menjaga gigi mereka tetap tajam");
                    intent.putExtra("HARGA_DEFAULT", "20000");
                    holder.itemView.getContext().startActivity(intent);
                }
                if (listMakanan.get(position).getNamaMakanan().equals("Pelet Ikan")) {
                    Intent intent = new Intent(holder.itemView.getContext(), MenuDetailActivity.class);
                    intent.putExtra("GAMBAR_DEFAULT", R.drawable.pelet_ikan);
                    intent.putExtra("NAMA_DEFAULT", "Pelet Ikan");
                    intent.putExtra("DESKRIPSI_DEFAULT", "Pilihan makanan yang mendukung kehidupan yang berkilau di dunia akuarium.");
                    intent.putExtra("HARGA_DEFAULT", "15000");
                    holder.itemView.getContext().startActivity(intent);
                }
                if (listMakanan.get(position).getNamaMakanan().equals("Makanan Burung")) {
                    Intent intent = new Intent(holder.itemView.getContext(), MenuDetailActivity.class);
                    intent.putExtra("GAMBAR_DEFAULT", R.drawable.makanan_burung);
                    intent.putExtra("NAMA_DEFAULT", "Makanan Burung");
                    intent.putExtra("DESKRIPSI_DEFAULT", "Campuran biji-bijian, buah-buahan segar, dan sayuran untuk memberikan penerbangan yang sempurna bagi burung peliharaan.");
                    intent.putExtra("HARGA_DEFAULT", "20000");
                    holder.itemView.getContext().startActivity(intent);
                }
                if (listMakanan.get(position).getNamaMakanan().equals("Makanan Serangga")) {
                    Intent intent = new Intent(holder.itemView.getContext(), MenuDetailActivity.class);
                    intent.putExtra("GAMBAR_DEFAULT", R.drawable.pakan_serangga);
                    intent.putExtra("NAMA_DEFAULT", "Makanan Serangga");
                    intent.putExtra("DESKRIPSI_DEFAULT", "Hidangan yang menggugah selera bagi reptil dan amfibi yang doyan serangga");
                    intent.putExtra("HARGA_DEFAULT", "15000");
                    holder.itemView.getContext().startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listMakanan.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtNamaMakanan, txtHargaMakanan;
        public ImageView imgMakanan;
        public ConstraintLayout itemView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txtNamaMakanan = (TextView) itemView.findViewById(R.id.txtNamaItem);
            txtHargaMakanan = (TextView) itemView.findViewById(R.id.txtHargaItem);
            imgMakanan = (ImageView) itemView.findViewById(R.id.imgItem);
            this.itemView = (ConstraintLayout) itemView.findViewById(R.id.itemLayout);
        }
    }
}
