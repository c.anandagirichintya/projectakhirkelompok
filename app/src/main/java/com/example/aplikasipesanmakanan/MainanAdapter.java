package com.example.aplikasipesanmakanan;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class MainanAdapter extends RecyclerView.Adapter<MainanAdapter.ViewHolder> {

    ViewHolder holder;

    public MainanAdapter(ArrayList<Mainan> listMainan) {
        this.listMainan = listMainan;
    }

    private ArrayList<Mainan> listMainan;

    @NonNull
    @Override

    public MainanAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewHolder holder = new ViewHolder(inflater.inflate(R.layout.item_menu, parent, false));

        return holder;
    }
    public String rp(int txt){
        Locale locale = new Locale("in", "ID");
        NumberFormat format = NumberFormat.getCurrencyInstance(locale);
        format.setMaximumFractionDigits(0);
        return format.format(txt); // Integer.toString(total);
    }

    @Override
    public void onBindViewHolder(@NonNull MainanAdapter.ViewHolder holder, int position) {
        Mainan mainan = listMainan.get(position);
        holder.txtNamaMainan.setText(mainan.getNamaMainan());
        holder.txtHargaMainan.setText(rp(Integer.parseInt(mainan.getHargaMainan())));
        holder.imgMainan.setImageResource(mainan.getImgMainan());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listMainan.get(position).getNamaMainan().equals("Bola")) {
                    Intent intent = new Intent(holder.itemView.getContext(), MenuDetailActivity.class);
                    intent.putExtra("GAMBAR_DEFAULT", R.drawable.bola);
                    intent.putExtra("NAMA_DEFAULT", "Bola");
                    intent.putExtra("DESKRIPSI_DEFAULT", "Bola berkilauan yang mengundang hewan peliharaan Anda untuk bermain sepanjang hari.");
                    intent.putExtra("HARGA_DEFAULT", "15000");
                    holder.itemView.getContext().startActivity(intent);
                }
                if (listMainan.get(position).getNamaMainan().equals("Mainan Interaktif")) {
                    Intent intent = new Intent(holder.itemView.getContext(), MenuDetailActivity.class);
                    intent.putExtra("GAMBAR_DEFAULT", R.drawable.mainan_interaktif);
                    intent.putExtra("NAMA_DEFAULT", "Mainan Interaktif");
                    intent.putExtra("DESKRIPSI_DEFAULT", "Permainan cerdas yang membuat hewan peliharaan Anda merasa seperti mereka sedang berhadapan dengan teman bermain");
                    intent.putExtra("HARGA_DEFAULT", "20000");
                    holder.itemView.getContext().startActivity(intent);
                }
                if (listMainan.get(position).getNamaMainan().equals("Mainan Rangsang Mental")) {
                    Intent intent = new Intent(holder.itemView.getContext(), MenuDetailActivity.class);
                    intent.putExtra("GAMBAR_DEFAULT", R.drawable.mainan_rangsangmental);
                    intent.putExtra("NAMA_DEFAULT", "Mainan Rangsang Mental");
                    intent.putExtra("DESKRIPSI_DEFAULT", "Tantangan otak yang menjaga hewan peliharaan Anda terlibat dan penasaran.");
                    intent.putExtra("HARGA_DEFAULT", "25000");
                    holder.itemView.getContext().startActivity(intent);
                }
                if (listMainan.get(position).getNamaMainan().equals("Mainan Berbulu")) {
                    Intent intent = new Intent(holder.itemView.getContext(), MenuDetailActivity.class);
                    intent.putExtra("GAMBAR_DEFAULT", R.drawable.mainan_bulu);
                    intent.putExtra("NAMA_DEFAULT", "Mainan Berbulu");
                    intent.putExtra("DESKRIPSI_DEFAULT", " Dibuat khusus untuk menggugah naluri pemburu kucing dengan bulu yang menggoda");
                    intent.putExtra("HARGA_DEFAULT", "10000");
                    holder.itemView.getContext().startActivity(intent);
                }
                if (listMainan.get(position).getNamaMainan().equals("Gigitan")) {
                    Intent intent = new Intent(holder.itemView.getContext(), MenuDetailActivity.class);
                    intent.putExtra("GAMBAR_DEFAULT", R.drawable.gigitananjing);
                    intent.putExtra("NAMA_DEFAULT", "Gigitan");
                    intent.putExtra("DESKRIPSI_DEFAULT", "Mainan kokoh yang bertahan lama untuk mengatasi naluri gigitan anjing");
                    intent.putExtra("HARGA_DEFAULT", "12000");
                    holder.itemView.getContext().startActivity(intent);
                }
                if (listMainan.get(position).getNamaMainan().equals("Mainan Laser")) {
                    Intent intent = new Intent(holder.itemView.getContext(), MenuDetailActivity.class);
                    intent.putExtra("GAMBAR_DEFAULT", R.drawable.laser_kucing);
                    intent.putExtra("NAMA_DEFAULT", "Mainan Laser");
                    intent.putExtra("DESKRIPSI_DEFAULT", "Sinar misterius yang memicu perburuan bersemangat kucing dengan cepat. ");
                    intent.putExtra("HARGA_DEFAULT", "30000");
                    holder.itemView.getContext().startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listMainan.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtNamaMainan, txtHargaMainan;
        public ImageView imgMainan;
        public ConstraintLayout itemView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txtNamaMainan = (TextView) itemView.findViewById(R.id.txtNamaItem);
            txtHargaMainan = (TextView) itemView.findViewById(R.id.txtHargaItem);
            imgMainan = (ImageView) itemView.findViewById(R.id.imgItem);
            this.itemView = (ConstraintLayout) itemView.findViewById(R.id.itemLayout);
        }
    }
}
