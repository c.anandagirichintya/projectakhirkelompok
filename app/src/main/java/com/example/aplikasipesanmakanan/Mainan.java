package com.example.aplikasipesanmakanan;

public class Mainan {
    private String namaMainan;
    String hargaMainan;
    private int imgMainan;

    public Mainan(String namaMainan, String hargaMainan, int imgMainan) {
        this.namaMainan = namaMainan;
        this.hargaMainan = hargaMainan;
        this.imgMainan = imgMainan;
    }

    public String getNamaMainan() {
        return namaMainan;
    }

    public void setNamaMainan(String namaMainan){ this.namaMainan = namaMainan;}

    public String getHargaMainan() {
        return hargaMainan;
    }
    public void setHargaMainan(String hargaMainan){this.hargaMainan = hargaMainan;} //ini no usage harusnya gaperlu isi jg gapapa
    public int getImgMainan() {
        return imgMainan;
    }
    public void setImgMainan(int imgMainan) {this.imgMainan = imgMainan;}

}
